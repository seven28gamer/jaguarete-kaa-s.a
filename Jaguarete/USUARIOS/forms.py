from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class RegistroForm(UserCreationForm):
    username = forms.CharField(
        label='Usuario', widget=forms.TextInput(attrs={'class': 'usuario todos', 'placeholder:': 'Ingrese su username aquí...'}), max_length=30, required=True)
    password1 = forms.CharField(
        label='Password', widget=forms.PasswordInput(attrs={'class': 'todos contra', 'placeholder:': 'Ingrese su contraseña aquí...'}), max_length=30, required=True)
    password2 = forms.CharField(
        label='Repetir Password', widget=forms.PasswordInput(attrs={'class' : ' todos contra1', 'placeholder:': 'Confirme su contraseña aquí...'}), max_length=30, required=True)
    first_name = forms.CharField(
        label='Nombre', widget=forms.TextInput(attrs={'class': 'nombre  todos', 'placeholder:': 'Ingrese su nombre aquí...'}), max_length=30, required=True)
    last_name = forms.CharField(
        label='Apellido', widget=forms.TextInput(attrs={'class': 'apellido  todos', 'placeholder:': 'Ingrese su apellido aquí...'}), max_length=30, required=True)
    email = forms.EmailField(
        label='Email', widget=forms.TextInput(attrs={'class': 'email todos', 'placeholder:': 'Ingrese su e-mail aquí...'}), max_length=254, required=True)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2')