from django.urls import path, include
from . import views

app_name = "eCommerce"
urlpatterns = [
    path('', views.index, name='index'),
    path('producto_alta', views.producto_alta, name="producto_alta")
    ]
