from django.shortcuts import render
from .models import Productos, Categoria
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from .forms import *

# Create your views here.

@login_required
def index(request):
    return render(request, "Productos/index.html", {
        "lista_Productos": Productos.objects.all(),
        "categorias": Categoria.objects.all()
    })

@permission_required('Productos.add_Productos')
def producto_alta(request):
    if request.method == "POST":
        form = FormProductos(request.POST)
        if form.is_valid():
            form.save()
    else:
        form = FormProductos()
        return render(request, "Productos/alta_producto.html", {
            "formset": form
        })
