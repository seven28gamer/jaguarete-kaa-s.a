from django import forms
from .models import Productos, Categoria

class FormProductos(forms.ModelForm):
    nombre = forms.CharField(
        label='nombre', widget=forms.TextInput(attrs={'class': 'imagen'}), max_length=64, required=True)
    descripcion = forms.CharField(
        label='descripcion', widget=forms.TextInput(attrs={'class': 'imagen'}), max_length=1000, required=True)
    categoria = forms.ChoiceField(
        label='categoria', widget=forms.Select(), choices=Categoria.objects.all, required=True)
    precio = forms.DecimalField(
        label='precio', widget=forms.NumberInput(attrs={'class': 'imagen'}), max_digits= 10, decimal_places=2, required=True)
    imagen = forms.URLField(
        label='imagen', widget=forms.URLInput(attrs={'class': 'imagen'}), max_length=30000, required=True)

    class Meta:
        model = Productos
        fields = ('nombre', 'descripcion', 'categoria', 'precio', 'imagen')



