from django.db import models
from django.db.models.fields import related


# Create your models here.

class Categoria(models.Model):
    nombre = models.CharField(max_length=64)
    destacada = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.nombre}"

    
    class Meta:
        db_table = 'categorias'
        verbose_name = 'Categoría'
        verbose_name_plural = 'Categorías'
        ordering = ['id'] 


class Productos(models.Model):   
    nombre = models.CharField(max_length=64)
    descripcion = models.CharField(max_length=1000)
    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE)
    precio = models.DecimalField(max_digits= 10, decimal_places=2, default=0)
    imagen = models.URLField(max_length=30000)
    

    def __str__(self):
        return f"El producto {self.nombre} de la categoria {self.categoria} cuesta ${self.precio}"

    class Meta:
        db_table = 'productos'
        verbose_name = 'Producto'
        verbose_name_plural = 'Productos'
        ordering = ['id'] 

